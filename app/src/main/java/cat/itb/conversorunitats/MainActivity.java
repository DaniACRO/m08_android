package cat.itb.conversorunitats;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText introdueixEditText;
    Spinner desplegableSpinner;
    Button calcularButton;
    TextView mostrarDatosEditText;
    double resultado;
    double medida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*++++++++ INICIALIZAR VARIABLES ++++++++++++*/
        introdueixEditText = (EditText)findViewById(R.id.dades_editText);
        desplegableSpinner = (Spinner)findViewById(R.id.choice_spinner);
        calcularButton = (Button)findViewById(R.id.calcular_button);
        mostrarDatosEditText = (TextView)findViewById(R.id.mostrar_datos_textView);


        /*+++++++++ ENLAZAR MENÚ SPINNER CON LOS ITEMS Y DARLE FORMATO +++++++++++++++++++++++++++++++*/
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.menu_spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        desplegableSpinner.setAdapter(adapter);


        /*++++++++++ KEYCODE BOTÓN INTRO ++++++++++++*/
        introdueixEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //Toast.makeText(MainActivity.this, "" + keyCode, Toast.LENGTH_SHORT).show();
                if (keyCode == 66){
                    CalcularMedida();
                }
                return false;
            }
        });


        /*++++++++++++ FUNCIONALIDAD DEL BOTÓN CALCULAR ++++++++++++++++++++*/
        calcularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalcularMedida();
            }
        });

    }

    /*++++++++ FUNCIÓN QUE CALCULA LA MEDIDA EN FUNCIÓN DE LA OPCIÓN SELECCIONADA +++++++++*/
    public void CalcularMedida(){
        if (introdueixEditText.getText().toString().isEmpty()){
            Toast.makeText(MainActivity.this, "Introdueix una dada numérica", Toast.LENGTH_SHORT).show();
        }else{
            medida = Double.parseDouble(introdueixEditText.getText().toString());  //recoger en un double el texto numérico introducido en el editText
            int position = desplegableSpinner.getSelectedItemPosition();  //recoger en un entero la posición seleccionada del Spinner
            switch (position) {
                case 0:
                    resultado = medida * 2.54;
                    break;
                case 1:
                    resultado = medida * 0.9144;
                    break;
                case 2:
                    resultado = medida * 1.60934;
                    break;
                case 3:
                    resultado = medida * 0.393701;
                    break;
                case 4:
                    resultado = medida * 1.09361;
                    break;
                case 5:
                    resultado = medida * 0.621371;
                    break;
            }
            mostrarDatosEditText.setText(String.format("%.2f", resultado));
            mostrarDatosEditText.setVisibility(View.VISIBLE);
        }
    }
}